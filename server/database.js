const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/Auth-User', {useNewUrlParser: true, useUnifiedTopology: true})
.then(()=> {
    console.log('Successfully connected!');
}).catch((err) => {
    console.log('error :', err);
});

module.exports = mongoose.connection;
