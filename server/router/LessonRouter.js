const express = require('express');

const LessonController = require('../http/Controllers/LessonController');
const AuthMiddleware = require("../http/Middlewares/AuthMiddleware");

const router = express.Router();

const prefix = '/lesson';

router.get('/', AuthMiddleware, LessonController.index);
router.post('/', AuthMiddleware, async (req, res) => {
    return await LessonController.save(req, res)
});
router.get('/:id', AuthMiddleware, LessonController.getById);
router.put('/:id', AuthMiddleware, LessonController.update);
router.delete('/:id', AuthMiddleware, LessonController.delete);

module.exports = {
    prefix,
    router
};
