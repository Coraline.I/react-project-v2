import http from "../http-common";

const AuthLogin = async (username, password) => {
    const res = await http
        .post("/login", {
            username,
            password,
        })
    console.log(res);
    if (res.data.token) {
        localStorage.setItem("user", JSON.stringify(res.data));
        localStorage.setItem("access_token", res.data.token);
    }
    return res.data
};

const AuthLogout = () => {
    localStorage.removeItem("user");
    window.location.href = "/";
}

export default AuthLogin;
