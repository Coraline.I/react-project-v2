import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

import Entete from "./Entete"


function Quizz1(){

  const [isValided, setIsValided] = useState(false);

  return(
    <div style={{display: 'flex', justifyContent: "center", alignItems: "center", flexDirection: "column"}}>
      <Entete formationName="lorem ipsum"></Entete>
      <div style={{borderBottom: "1px solid #DFDFDF", width:"90%", display: 'flex', flexDirection: "column" ,marginTop: "30px"}}>
        <div style={{borderBottom: "1px solid #DFDFDF", width: "100%", display: "flex"}}>
          <h2>Lesson 1 lorem ipsum</h2>
        </div>
        <div style={{marginBottom: "25px"}}>
          {/*<MeryXmas></MeryXmas>*/}
        </div>
      </div>
      <div style={{width: "90%" , display: 'flex', flexDirection: "column", justifyContent: "center", alignItems: "center"}}>
        <div style={{borderTop: "solid 6px black"}}>
          <h2>Lorem ipsum titre quiz</h2>
        </div>
        <div style={{marginBottom: "100px", width: "60%", display: 'flex', flexDirection: "column", alignItems: "start", marginTop: "30px"}}>
          <h3>Question lorem ipsum solet amet?</h3>
          <div style={{display: 'flex', flexDirection: "column", width: "100%"}}>
            <div data-r={false} onClick={(e)=>{
              setIsClicked1(true)
              setIsClicked2(false)
              setIsClicked3(false)
            }} style={{border: `3px solid ${isClicked1 && isValided && r1 === "true" ? "#E5E5E5" : isClicked1 && isValided && r1 === "false" ? "#000000": isClicked1 ? "black": "#CBCBCB" }`, marginTop: "20px", width: "90%", height: "75px", display: "flex", justifyContent: "space-between", alignItems: 'center'}}>
              <p style={{marginBottom: "0px", marginLeft: "30px"}}>Lorem ipsum reponse 1 solet amet?</p>
              {isValided && r1 === "true" ? <p style={{marginBottom: "0px", marginRight: "30px", color: "#17C45C"}}>la bonne reponse</p> : isClicked1 && isValided && r1 == "false" ? <p style={{marginBottom: "0px", marginRight: "30px", color: "#FF0000"}}>Erreur</p>: isClicked1 ? <img style={{marginRight: "30px", height: "25px", width: "25px"}} src="./Vector.png" alt=""/>: null }

            </div>

            <div data-r={false} onClick={(e)=>{
              setIsClicked1(false)
              setIsClicked2(true)
              setIsClicked3(false)
            }} style={{border: `3px solid ${isClicked2 && isValided && r2 === "true" ? "#E5E5E5" : isClicked2 && isValided && r2 == "false" ? "#000000": isClicked2 ? "black": "#CBCBCB" }`, marginTop: "20px", width: "90%", height: "75px", display: "flex", justifyContent: "space-between", alignItems: 'center'}}>
              <p style={{marginBottom: "0px", marginLeft: "30px"}}>Lorem ipsum reponse 1 solet amet?</p>
              {isValided && r2 === "true" ? <p style={{marginBottom: "0px", marginRight: "30px", color: "#17C45C"}}>la bonne reponse</p>: isClicked2 && isValided && r2 == "false" ? <p style={{marginBottom: "0px", marginRight: "30px", color: "#FF0000"}}>Erreur</p>: isClicked2 ? <img style={{marginRight: "30px", height: "25px", width: "25px"}} src="./Vector.png" alt=""/>: null }
            </div>

            <div data-r={true} onClick={(e)=>{
              setIsClicked1(false)
              setIsClicked2(false)
              setIsClicked3(true)
            }} style={{border: `3px solid ${isValided && r3 === "true" ? "#E5E5E5" : isClicked3 && isValided && r3 == "false" ? "#000000": isClicked3 ? "black": "#CBCBCB" }`, marginTop: "20px", width: "90%", height: "75px", display: "flex", justifyContent: "space-between", alignItems: 'center'}}>
              <p style={{marginBottom: "0px", marginLeft: "30px"}}>Lorem ipsum reponse 1 solet amet?</p>
              {isValided && r3 === "true" ? <p style={{marginBottom: "0px", marginRight: "30px", color: "#17C45C"}}>la bonne reponse</p> : isClicked3 && isValided && r3 == "false" ? <p style={{marginBottom: "0px", marginRight: "30px", color: "#FF0000"}}>Erreur</p>: isClicked3 ? <img style={{marginRight: "30px", height: "25px", width: "25px"}} src="./Vector.png" alt=""/>: null }
            </div>
          </div>
        </div>
      </div>
      <div style={{display: "flex", justifyContent: "center", alignItems: "center",height: "110px", width: "100%", backgroundColor: "black"}}>
        <button onClick={(e)=>{
          setIsValided(true)
        }} style={{border: "none", borderRadius: '50px', width: "200px", height: "50px", backgroundColor: "white"}}>Valider étape</button>
      </div>
    </div>
  )
}

export default Quizz1;
