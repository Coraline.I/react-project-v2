import React, {useEffect, useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import Entete from "./Entete";
import AddNameFormation from "./addNameFormation";
//import AddNameLesson from "./addNameLesson"
import PlayVideo from "./PlayVideo";
import Footer from './Footer';
import {useParams} from "react-router-dom";
import axios from "axios";

function Formations() {

    const [lesson, setLesson] = useState('');

    let id = useParams();

    useEffect(async () => {
        const result = await axios.get('http://159.65.28.230:3080/lesson/' + id.id, {headers: {Authorization: 'Bearer ' + localStorage.getItem('access_token')}});
        setLesson(await result.data);
    }, [])

    console.log(lesson);

    return (
        <div>
            <Entete formationName={lesson.title}></Entete>
            <AddNameFormation content={lesson.Content}></AddNameFormation>
            <PlayVideo content={lesson.Video}></PlayVideo>
            <Footer></Footer>
        </div>
    )
}

export default Formations;
