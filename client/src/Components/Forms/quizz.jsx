import React, {useState} from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import axios from "axios";
import {useParams} from "react-router-dom";

function QuizzForm(props) {

    let id = useParams();
    const update = async () => {
        const result = await axios.put('http://159.65.28.230:3080/lesson/' + id.id, {Video: {
                title: inputTitle,
                question: inputQuestion,
                answers:answers
            },} ,{headers: {Authorization: 'Bearer ' + localStorage.getItem('access_token')}});
        console.log(result);
    }

    console.log(props.content.answers);
    const [inputTitle, setInputTitle] = useState(props.content.title);
    const [inputQuestion, setInputQuestion] = useState(props.content.question);
    const [answers, setAnswers] = useState([
        {
            correct: false,
            text: "Réponse 1 ...",
            id: 0
        },

        {
            correct: false,
            text: "Réponse 2 ...",
            id: 1
        },

        {
            correct: false,
            text: "Réponse 3 ...",
            id: 2
        }
    ]);

    /* onChange input title & question ------------------------------------------*/
    function handleInputsChange(event, nb) {
        if (nb === 0) {
            setInputTitle(event.target.value)
        } else {
            setInputQuestion(event.target.value)
        }
    }

    /* onChange answers ----------------------------------------------------------*/
    const handleAnswersChange = (e, index) => {
        const {name, value} = e.target;
        const change = [...answers];
        change[index][name] = value;
        setAnswers(change);
    };

    function handleChekboxChange(e, index) {
        const getPosition = answers.findIndex(function (a) {
            return a.id === index;
        });
        const change = [...answers];
        change[getPosition].correct = e.target.checked;
        setAnswers(change);
    };


    return (
        <div className="container-fluid w-75">
            <h2 className="row my-3 justify-content-center title1">Quizz</h2>
            <div>

                <div className="row justify-content-center">
                    <Form.Control className="formbg form3" type="title" name="title"
                                  placeholder={'Titre quizz ...'} value={inputTitle}
                                  onChange={(event) => handleInputsChange(event, 0)}/>
                </div>

                <div className="row my-2 justify-content-center">
                    <Form.Control className="formbg form3" type="text" name="question" placeholder={'Question ...'} value={inputQuestion}
                                  onChange={(event) => handleInputsChange(event, 1)}/>
                </div>

                {
                    answers.map((item, index) => {
                        return (
                            <>
                                <div className="row mt-4 justify-content-center">
                                    <Form.Control className="formbg form3" type="text" name="text"
                                                  placeholder={item.text}
                                                  onChange={e => handleAnswersChange(e, index)}/>
                                </div>

                                <div className="row">
                                    <input type="checkbox" id={`link-${index}`} onChange={event => handleChekboxChange(event, index)}/>
                                    <label htmlFor={`link-${index}`} className="my-2">Bonne réponse</label>
                                </div>
                            </>
                        )
                    })
                }


                <div className="my-4 line2"/>
            </div>

            <div className="row">
                <Button
                    className="addbutton border-0"
                    onClick={(event) => {
                        const id = answers.length;
                        const addAnswer = {
                            correct: false,
                            label: `Réponse ${id + 1} ...`,
                            id
                        };
                        setAnswers([...answers, addAnswer])
                    }}
                >+ ajouter réponse
                </Button>

            </div>
            <div className="row my-5 justify-content-center">
                <Button className="registerbutton" block size="lg" type="submit"
                        onClick={update}>Enregistrer</Button>
            </div>
        </div>
    )

}

export default QuizzForm;

