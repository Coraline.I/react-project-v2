import React, {useState} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button} from 'react-bootstrap';
import Form from "react-bootstrap/Form";
import axios from "axios";
import {useParams} from "react-router-dom";

function HeaderForm() {

    const [name, setName] = useState('');

    let id = useParams();
    const update = async () => {
        await axios.put('http://159.65.28.230:3080/lesson/' + id.id, {status:true} ,{headers: {Authorization: 'Bearer ' + localStorage.getItem('access_token')}});
    }

    return (
        <>
            <div className="container-fluid header-bg">
                <div className="row py-4 px-4 justify-content-between">
                    <div className="row align-items-center">
                        <h2 className="mx-4 title1 text-dark">Editeur de formation</h2>
                        <Form.Control type="text" className="formbg form2 mr-4"
                                      onChange={((event) => {
                                          setName(event.target.value)
                                      })}
                                      placeholder="Nom de la formation..."/>
                        <Button className="registerbutton" block size="lg" type="submit">Enregistrer</Button>
                    </div>
                    <div className="row align-items-center mr-4">
                        <Button className="publish-button border-0 mr-1" onClick={update}>Publier</Button>
                        <Button className="logout-button" onClick={() => {
                            localStorage.removeItem("user");
                            window.location.href = "/";
                        }} >Déconnexion</Button>
                    </div>
                </div>
            </div>
        </>
    )
}

export default HeaderForm;