 import React, {Component} from "react";
import Button from "react-bootstrap/Button";
import ContentForm from "../Forms/content";
import QuizzForm from "../Forms/quizz";
import VideoForm from "../Forms/video";
import icon1 from "../assets/texte.svg"
import icon2 from "../assets/quiz.svg"
import icon3 from "../assets/video.svg"

class ButtonList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            openedComponent: undefined,
        };
    }

    toggleComponent(name) {
        console.log(name);
        if (this.state.openedComponent === name) {
            this.setState({openedComponent: undefined});
        } else {
            this.setState({openedComponent: name});
        }
    }

    render() {
        const {openedComponent} = this.state;
        console.log(this.props.lesson);
        return <div className="container-fluid my-4">
            {
                <div>
                    {openedComponent === "content" && <ContentForm content={this.props.lesson.Content}/>}
                    {openedComponent === "quizz" && <QuizzForm content={this.props.lesson.Quiz}/>}
                    {openedComponent === "video" && <VideoForm content={this.props.lesson.Video}/>}
                </div>
            }
            <div className="row justify-content-center">
                <h1 className="title2 title3 mb-5">+ ajout contenu lesson</h1>
            </div>
            <div className="row justify-content-center">
                <Button className="buttonlist mr-2" onClick={() => this.toggleComponent("content")}>
                    <img src={icon1} alt="imgfailed"/>
                    <br/>
                    Contenu
                </Button>
                <Button className="buttonlist mr-2" onClick={() => this.toggleComponent("quizz")}>
                    <img src={icon2} alt="imgfailed"/>
                    <br/>
                    Quiz
                </Button>
                <Button className="buttonlist mb-5" onClick={() => this.toggleComponent("video")}>
                    <img src={icon3} alt="imgfailed"/>
                    <br/>
                    Video
                </Button>
            </div>
        </div>
    }
}

export default ButtonList;
