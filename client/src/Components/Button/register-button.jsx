import React from "react";
import Button from "react-bootstrap/Button";

export default function RegisterButton (props) {
    return (
        <Button className="registerbutton" type="submit">Enregistrer</Button>
    )
}
