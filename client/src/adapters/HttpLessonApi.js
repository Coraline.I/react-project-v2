import axios from "axios";

class HttpLessonApi {
    http;

    constructor() {
        this.http = axios.create({
            baseURL: 'http://159.65.28.230:3080/'
        }).interceptors.request.use(function (config) {
            config.headers.Authorization = `Bearer  ${localStorage.getItem("access_token")}`
            return config;
        }, function (error) {
            return Promise.reject(error);
        })
    }

    async save(body) {
        return await this.http.post('/lesson', body);
    }

    async getById(body) {
        return await this.http.get('/lesson/:id', body);
    }

    async update(body) {
        return await this.http.put('/lesson/:id', body);
    }

    async delete(body) {
        return await this.http.delete('/lesson/:id', body);
    }
}

module.exports = new HttpLessonApi();